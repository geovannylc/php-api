# S360 Technical Test - API (PHP/Laravel codebase)
Hello! Thank you for taking the time to complete this test. Below, you will find instructions on how to complete the API
portion of the tech test. Please follow the setup instructions to get the API up and running. If you run into issues 
getting the application to work, please reach out to Eric Schmiedel [eschmiedel@storage360.com](mailto:eschmiedel@storage360.com)
for further assistance.

## Goals
The goal of this portion of the tests is to demonstrate your knowledge of working with Laravel and PHP. It is also meant
to test your ability to quickly learn conventions of a codebase, follow those conventions for the sake of consistency,
and test your ability to debug.

## Setup Instructions
This setup assumes that you have a development environment that is capable of running Docker, a terminal, and knowledge
on how to use the terminal.

1. Fork this repository so that you have your own version in your GitLab account
   * Make sure that you mark your repository as public so that we can review it
2. Open your terminal and clone your fork (**DO NOT CLONE THE MAIN REPOSITORY!**)
3. `$ cp .env.example .env` (the example contains everything you need and needs no settings changed)
4. `$ docker run --rm -v "$(pwd)":/opt -w /opt composer:2 bash -c "composer install"`
5. `$ vendor/bin/sail up`
6. `$ vendor/bin/sail artisan key:generate`
7. `$ vendor/bin/sail artisan migrate --seed`
8. `$ chmod -R 0777 bootstrap storage`
9. Visit [http://localhost:8042/api/v1](http://localhost:8042/api/v1) and you should see a response

## API Description & Tasks
This is a simple, basic REST API for animal facts. Currently, Cat Facts is the only resource this API provides. You have
two tasks to complete. Before you get started on these tasks, I would recommend exploring the codebase a little bit to 
understand the routes, file structure, and overall architecture.

### Task 1 - Storing a Cat Fact is Broken!
Someone made a mistake and pushed their code without running tests. Now, you have to fix their mistake. Once you have
identified the issue and implemented the fix, all the Feature tests for the CatFacts endpoints should pass.

You can run `$ vendor/bin/sail test` to run the feature tests. **All feature tests must pass in order to consider this a success**.

### Task 2 - Storing Dog Facts
Our product owner has tasked us with our next product enhancement, and you will be responsible for implementing the feature.
**Following the conventions in place** for working with Cat Facts, create the necessary infrastructure and endpoints to handle 
Dog Facts. This is an API, so all responses should be in JSON, and any request that accepts data should expect it to be
in the JSON format. You should also create Feature tests for your endpoints. Below is the user story for Dog Facts.

#### Dog Facts User Story
As a user of the API, I should be able to list, retrieve, store, update, and delete Dog Facts so that I can help others
learn about Dogs.

**Acceptance Criteria**
* I should be able to retrieve a paginated list of Dog Facts
  * I should be able to specify the amount of Dog Facts per page
  * The default number of Dog Facts should be 25 (just like Cat Facts)
  * I should be able to request additional pages of Dog Facts
* I should be able to retrieve a specific Dog Fact
* I should be able to store a Dog Fact
  * The fact text is required in order to store the Dog Fact
* I should be able to update a Dog Fact
  * The fact text is required in order to update the Dog Fact
* I should be able to delete a Dog Fact

## Submission
After you have completed the tasks, please commit your local files and **push them up to your fork**. Notify the person
you have been working with that you have completed the test and provide them a link to **your** fork.
