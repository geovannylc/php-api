<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\DogFact;
use Illuminate\Pagination\LengthAwarePaginator;

class DogFactRepository
{
    /**
     * @param DogFact $dogFact
     */
    public function __construct(private readonly DogFact $dogFact)
    {
    }

    /**
     * @param array|null $options
     * @param array|null $relations
     * @return LengthAwarePaginator|null
     */
    public function getAllWithPagination(
        ?array $options = [],
        ?array $relations = []
    ): ?LengthAwarePaginator {
        $pageSize = $options['pageSize'] ?? config('pagination.default_size');

        return $this->dogFact->with($relations)->paginate($pageSize)->withQueryString();
    }

    /**
     * @param int $id
     * @param array $relations
     * @return DogFact|array|null
     */
    public function getById(int $id, array $relations = []): DogFact|array|null
    {
        return $this->dogFact
            ->with($relations)
            ->findOrFail($id);
    }

    /**
     * @param array $data
     * @return DogFact|null
     */
    public function store(array $data): ?DogFact
    {
        return $this->dogFact->create($data);
    }

    /**
     * @param DogFact $dogFact
     * @param array $data
     * @return bool
     */
    public function update(DogFact $dogFact, array $data): bool
    {
        return $dogFact->update($data);
    }

    /**
     * @param DogFact $dogFact
     * @return bool|null
     */
    public function destroy(DogFact $dogFact): ?bool
    {
        return $dogFact->delete();
    }
}
