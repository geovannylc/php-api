<?php

namespace App\Models;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      title="DogFact",
 *      description="Eloquent DogFact Model",
 *      @OA\Xml(
 *          name="DogFact",
 *      )
 *  )
 * @OA\Property(
 *        description="DogFact's id",
 *          type="integer",
 *        example="999",
 *     property="id"
 *  )
 * @OA\Property(
 *       description="fact",
 *       example="Some fact about dogs",
 *     type="string",
 *     property="fact"
 * )
 */
class DogFact extends BaseModel
{
    protected $fillable = ['fact'];

    protected $visible = ['id', 'fact'];
}
