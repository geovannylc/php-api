<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\DogFactRepository;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *     description="Api documentation",
 *     version="1.0.0",
 *     title="API Test",
 *     termsOfService="http://swagger.io/terms/",
 *     @OA\Contact(
 *         email="geovannylc@gmail.com"
 *     )
 * )
 * @OA\Tag(
 *     name="CatFacts",
 *     description="cat facts"
 * )
 * @OA\Tag(
 *     name="DogFacts",
 *     description="Dog facts"
 * )
 * @OA\Server(
 *      description="Local Server - Api Test",
 *      url=""
 *  )
 */
abstract class ApiController extends Controller
{
    protected const UPDATE_ERROR_MESSAGE = 'Dog fact could not be updated';
    protected const STORE_ERROR_MESSAGE = 'Dog fact could not be created';
    protected const DESTROY_ERROR_MESSAGE = 'Dog fact could not be deleted';

    /**
     * @param DogFactRepository $dogFactRepository
     */
    public function __construct(protected readonly DogFactRepository $dogFactRepository)
    {
    }
}
