<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\ApiController;
use App\Http\Requests\API\V1\DogFacts\Rest\UpdateDogFactRequest;
use App\Models\DogFact;
use Exception;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;

class UpdateController extends ApiController
{
    /**
     * @OA\Put(
     *       path="/api/v1/dog-facts",
     *       description="Update a dog fact",
     *       tags={"DogFacts"},
     *       summary="Update a dog fact",
     *   @OA\RequestBody(
     *           required=true,
     *           description="Update DogFact Request Schema",
     *           @OA\MediaType(
     *               mediaType="application/json",
     *               @OA\Schema(ref="#/components/schemas/UpdateDogFactRequest")
     *           )
     *       ),
     *    @OA\Response(
     *       response=201,
     *       description="Success",
     *       @OA\JsonContent(
     *          ref="#/components/schemas/DogFact")
     *        ),
     *      )
     *  )
     * @param DogFact $dogFact
     * @param UpdateDogFactRequest $request
     * @return JsonResponse
     */
    public function __invoke(DogFact $dogFact, UpdateDogFactRequest $request): JsonResponse
    {
        try {
            $updated = $this->dogFactRepository->update($dogFact, $request->all());

            if (!$updated) {
                abort(500, self::UPDATE_ERROR_MESSAGE);
            }

        } catch (Exception $exception) {
            abort(500, $exception->getMessage());
        }

        return new JsonResponse($dogFact, 200);
    }
}
