<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

/**
 * @OA\Get(
 *       path="/api/v1/dog-facts",
 *       description="Dog facts list",
 *       tags={"DogFacts"},
 *       summary="Dog facts list",
 *       @OA\Parameter(
 *           name="page",
 *           description="page number",
 *           required=false,
 *           in="query",
 *           @OA\Schema(
 *               type="integer"
 *           )
 *       ),
 *       @OA\Parameter(
 *            name="pageSize",
 *            description="amount of items per page",
 *            required=false,
 *            in="query",
 *            @OA\Schema(
 *                type="integer"
 *            )
 *       ),
 *       @OA\Response(
 *       response=200,
 *       description="Success",
 *       @OA\JsonContent(
 *          ref="#/components/schemas/DogFactPaginated")
 *        ),
 *      )
 *  )
 */
class IndexController extends ApiController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        return new JsonResponse($this->dogFactRepository
            ->getAllWithPagination($request->all()));
    }
}
