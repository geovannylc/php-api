<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\ApiController;
use App\Models\DogFact;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;

class ShowController extends ApiController
{
    /**
     * @OA\Get(
     *         path="/api/v1/dog-facts/{id}",
     *         description="Get a dog fact",
     *         tags={"DogFacts"},
     *      summary="Get a dog fact",
     *     @OA\Parameter(
     *           required=true,
     *           name="id",
     *           in="path",
     *     ),
     *      @OA\Response(
     *         response=200,
     *         description="Success",
     *     @OA\JsonContent(
     *           ref="#/components/schemas/DogFact")
     *         ),
     *       )
     *        )
     *    )
     * @param DogFact $dogFact
     * @return JsonResponse
     */
    public function __invoke(DogFact $dogFact): JsonResponse
    {
        return new JsonResponse($dogFact, 200);
    }
}
