<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\ApiController;
use App\Models\DogFact;
use Exception;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;

class DestroyController extends ApiController
{
    /**
     * @OA\Delete(
     *        path="/api/v1/dog-facts/{id}",
     *        description="Delete a dog fact",
     *        tags={"DogFacts"},
     *     summary="Delete a dog fact",
     *    @OA\Parameter(
     *          required=true,
     *          name="id",
     *          in="path",
     *    ),
     *     @OA\Response(
     *        response=200,
     *        description="Success",
     *       )
     *   )
     * @param DogFact $dogFact
     * @return JsonResponse
     */
    public function __invoke(DogFact $dogFact): JsonResponse
    {
        try {
            $deleted = $this->dogFactRepository->destroy($dogFact);
        } catch (Exception $exception) {
            abort(500, $exception->getMessage());
        }

        if (!$deleted) {
            abort(500, self::DESTROY_ERROR_MESSAGE);
        }

        return new JsonResponse($deleted, 204);
    }
}
