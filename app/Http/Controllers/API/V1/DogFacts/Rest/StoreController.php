<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\ApiController;
use App\Http\Requests\API\V1\DogFacts\Rest\StoreDogFactRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;

final class StoreController extends ApiController
{
    /**
     * @OA\Post(
     *      path="/api/v1/dog-facts",
     *      description="Store dog facts",
     *      tags={"DogFacts"},
     *   summary="Create a dog fact",
     *  @OA\RequestBody(
     *          required=true,
     *          description="Create DogFact Request Schema",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/StoreDogFactRequest")
     *          )
     *      ),
     *   @OA\Response(
     *      response=201,
     *      description="Success",
     *      @OA\JsonContent(
     *         ref="#/components/schemas/DogFact")
     *       ),
     *     )
     * )
     * @param StoreDogFactRequest $request
     * @return JsonResponse
     */
    public function __invoke(StoreDogFactRequest $request): JsonResponse
    {
        try {
            $dogFact = $this->dogFactRepository->store($request->all());
        } catch (Exception $exception) {
            abort(500, $exception->getMessage());
        }

        if (!$dogFact) {
            abort(500, self::STORE_ERROR_MESSAGE);
        }

        return new JsonResponse($dogFact, 201);
    }
}
