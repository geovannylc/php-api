<?php

namespace App\Http\Requests\API\V1\DogFacts\Rest;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *       title="StoreDogFactRequest",
 *       description="Store DogFact Request",
 *       required={"fact"},
 *       @OA\Xml(
 *           name="StoreDogFactRequest",
 *       )
 *   )
 * @OA\Property(
 *        description="fact",
 *        example="Some fact about dogs",
 *        type="string",
 *        property="fact"
 *  )
 */
class StoreDogFactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'fact' => 'required|string',
        ];
    }
}
