<?php

namespace App\Http\Requests\API\V1\DogFacts\Rest;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UpdateDogFactRequest",
 *     description="Update DogFact Request Schema",
 *     required={"fact"},
 *     @OA\Xml(
 *         name="UpdateDogFactRequest",
 *     ),
 * )
 * @OA\Property(
 *         description="Tell a fact about dogs",
 *         type="string",
 *         example="Dog's are friendly",
 *         property="fact"
 *   )
 */
class UpdateDogFactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'fact' => 'required|string',
        ];
    }
}
