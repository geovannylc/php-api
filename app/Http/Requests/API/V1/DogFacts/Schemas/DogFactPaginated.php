<?php

namespace App\Http\Requests\API\V1\DogFacts\Schemas;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      title="DogFactPaginated",
 *      description="Dog Fact Paginated",
 *      @OA\Xml(
 *          name="DogFactPaginated",
 *      ),
 *      allOf={
 *        @OA\Schema(ref="#/components/schemas/LaravelPaginator"),
 *        @OA\Schema(
 *            required={"data"},
 *            @OA\Property(property="data", type="array",
*              @OA\Items(ref="#/components/schemas/DogFact"))
 *        )
 *    }
 *  )
 */
class DogFactPaginated
{
}
