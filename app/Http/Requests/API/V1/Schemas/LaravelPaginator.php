<?php

namespace App\Http\Requests\API\V1\Schemas;

use OpenApi\Annotations as OA;

/**
 *  @OA\Schema (
 *     title="LaravelPaginator",
 *     description="Laravel Paginator",
 *     @OA\Xml(
 *         name="LaravelPaginator"
 *     ),
 *     @OA\Property(
 *      property="total",
 *      type="integer",
 *      description="Total of items"
 *      ),
 *     @OA\Property(
 *      property="per_page",
 *      type="integer",
 *      description="Items per page"
 *      ),
 *     @OA\Property(
 *      property="current_page",
 *      type="integer",
 *      description="Current page number"
 *      ),
 *     @OA\Property(
 *      property="last_page",
 *      type="integer",
 *      description="Last page number"
 *      ),
 *     @OA\Property(
 *      property="first_page_url",
 *      type="string",
 *      description="First page url"
 *      ),
 *     @OA\Property(
 *      property="last_page_url",
 *      type="string",
 *      description="Last page url"
 *      ),
 *      @OA\Property(
 *      property="prev_page_url",
 *      type="string",
 *      description="Next page url",
 *      nullable="true"
 *      ),
 *      @OA\Property(
 *      property="path",
 *      type="string",
 *      description="URL API path"
 *      ),
 *      @OA\Property(
 *      property="from",
 *      type="integer",
 *      ),
 *      @OA\Property(
 *      property="to",
 *      type="integer",
 *      ),
 *      @OA\Property(
 *      property="data",
 *      type="array",
 *      @OA\Items(type="object"),
 *      description="List of data"
 *      )
 * )
 */
class LaravelPaginator
{
}
