<?php

declare(strict_types=1);

namespace Tests\Feature\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\V1\DogFacts\Rest\StoreController;
use App\Models\DogFact;
use App\Repositories\DogFactRepository;
use Tests\Feature\API\V1\DogFacts\DogFactTestCase;

class StoreControllerTest extends DogFactTestCase
{
    protected string $fact = 'This is a test dog fact from the StoreControllerTest';

    public function testSuccess(): void
    {
        $response = $this->postJson(
            $this->getRouteURL(),
            [
            'fact' => $this->fact,
        ]
        );

        $response->assertStatus(201);

        $this->assertArrayHasKey('id', $response->json());
        $this->assertArrayHasKey('fact', $response->json());


        DogFact::find($response->json('id'))->forceDelete();
    }

    public function testValidationFailure(): void
    {
        $response = $this->postJson($this->getRouteURL(), []);

        $response->assertStatus(422);

        $response2 = $this->postJson($this->getRouteURL(), [
            'fact' => null,
        ]);

        $response2->assertStatus(422);

        $response3 = $this->postJson($this->getRouteURL(), [
            'fact' => 123,
        ]);

        $response3->assertStatus(422);
    }

    public function testHandlesExceptionDuringSave(): void
    {
        $this->app->bind(StoreController::class, function () {
            $mock = $this->getMockBuilder(DogFactRepository::class)
                ->onlyMethods(['store'])
                ->getMock();

            $mock->expects($this->once())
                ->method('store')
                ->willThrowException(new \Exception('Some DB error'));

            return new StoreController($mock);
        });

        $response = $this->postJson($this->getRouteURL(), [
            'fact' => $this->fact,
        ]);

        $response->assertStatus(500);
    }

    public function testHandlesFailureToSave(): void
    {
        $this->app->bind(StoreController::class, function () {
            $mock = $this->getMockBuilder(DogFactRepository::class)
                ->onlyMethods(['store'])
                ->getMock();

            $mock->expects($this->once())
                ->method('store')
                ->willReturn(null);

            return new StoreController($mock);
        });

        $response = $this->postJson($this->getRouteURL(), [
            'fact' => $this->fact,
        ]);

        $response->assertStatus(500);
    }
}
