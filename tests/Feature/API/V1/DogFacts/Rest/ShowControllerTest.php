<?php

declare(strict_types=1);

namespace Tests\Feature\API\V1\DogFacts\Rest;

use Tests\Feature\API\V1\DogFacts\DogFactTestCase;

class ShowControllerTest extends DogFactTestCase
{
    protected string $fact = 'This is a dog fact';

    public function testSuccess(): void
    {
        $response = $this->getJson($this->getRouteURL($this->dogFact->id));

        $response->assertStatus(200);
        $response->assertJson([
            'id' => $this->dogFact->id,
            'fact' => $this->dogFact->fact,
        ]);

    }

    public function testNotFound(): void
    {
        $response = $this->getJson($this->getRouteURL(6942069));

        $response->assertStatus(404);
    }
}
