<?php

declare(strict_types=1);

namespace Tests\Feature\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\V1\DogFacts\Rest\DestroyController;
use App\Models\DogFact;
use App\Repositories\DogFactRepository;
use Tests\Feature\API\V1\DogFacts\DogFactTestCase;

class DestroyControllerTest extends DogFactTestCase
{
    public function testSuccess(): void
    {
        $response = $this->deleteJson($this->getRouteURL($this->dogFact->id));

        $response->assertStatus(204);

        $this->assertNull(DogFact::find($this->dogFact->id));
    }

    public function testNotFound(): void
    {
        $response = $this->deleteJson($this->getRouteURL(6942069));

        $response->assertStatus(404);
    }

    public function testHandlesExceptionDuringDelete(): void
    {
        $this->app->bind(DestroyController::class, function () {
            $mock = $this->getMockBuilder(DogFactRepository::class)
                ->onlyMethods(['destroy'])
                ->getMock();

            $mock->expects($this->once())
                ->method('destroy')
                ->willThrowException(new \Exception('Some DB error'));

            return new DestroyController($mock);
        });

        $response = $this->deleteJson($this->getRouteURL($this->dogFact->id));

        $response->assertStatus(500);
    }

    public function testHandlesFailureToDelete(): void
    {
        $this->app->bind(DestroyController::class, function ($app) {
            $mock = $this->getMockBuilder(DogFactRepository::class)
                ->onlyMethods(['destroy'])
                ->getMock();

            $mock->expects($this->once())
                ->method('destroy')
                ->willReturn(false);

            return new DestroyController($mock);
        });

        $response = $this->deleteJson($this->getRouteURL($this->dogFact->id));

        $response->assertStatus(500);
    }
}
