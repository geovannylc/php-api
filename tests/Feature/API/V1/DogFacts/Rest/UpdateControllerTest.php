<?php

declare(strict_types=1);

namespace Tests\Feature\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\V1\DogFacts\Rest\UpdateController;
use App\Repositories\DogFactRepository;
use Tests\Feature\API\V1\DogFacts\DogFactTestCase;

class UpdateControllerTest extends DogFactTestCase
{
    protected string $updatedFact = 'This is a the updated fact';

    public function testSuccess(): void
    {
        $response = $this->putJson(
            $this->getRouteURL($this->dogFact->id),
            [
            'fact' => $this->updatedFact,
        ]
        );

        $response->assertStatus(200);

        $response->assertJson([
            'id' => $this->dogFact->id,
            'fact' => $this->updatedFact,
        ]);
    }

    public function testNotFound()
    {
        $response = $this->putJson($this->getRouteURL(6942069), [
            'fact' => $this->updatedFact,
        ]);

        $response->assertStatus(404);
    }

    public function testValidationFailure(): void
    {
        $response = $this->putJson($this->getRouteURL($this->dogFact->id), []);

        $response->assertStatus(422);

        $response2 = $this->putJson($this->getRouteURL($this->dogFact->id), [
            'fact' => null,
        ]);

        $response2->assertStatus(422);

        $response3 = $this->putJson($this->getRouteURL($this->dogFact->id), [
            'fact' => 123,
        ]);

        $response3->assertStatus(422);
    }

    public function testHandlesExceptionDuringSave(): void
    {
        $this->app->bind(UpdateController::class, function () {
            $mock = $this->getMockBuilder(DogFactRepository::class)
                ->onlyMethods(['update'])
                ->getMock();

            $mock->expects($this->once())
                ->method('update')
                ->willThrowException(new \Exception('Some DB error'));

            return new UpdateController($mock);
        });

        $response = $this->putJson($this->getRouteURL($this->dogFact->id), [
            'fact' => $this->updatedFact,
        ]);

        $response->assertStatus(500);
    }

    public function testHandlesFailureToSave(): void
    {
        $this->app->bind(UpdateController::class, function () {
            $mock = $this->getMockBuilder(DogFactRepository::class)
                ->onlyMethods(['update'])
                ->getMock();

            $mock->expects($this->once())
                ->method('update')
                ->willReturn(null);

            return new UpdateController($mock);
        });

        $response = $this->putJson($this->getRouteURL($this->dogFact->id), [
            'fact' => $this->updatedFact,
        ]);

        $response->assertStatus(500);
    }
}
