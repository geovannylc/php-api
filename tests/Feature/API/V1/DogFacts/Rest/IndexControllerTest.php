<?php

declare(strict_types=1);

namespace Tests\Feature\API\V1\DogFacts\Rest;

use Tests\Feature\API\V1\DogFacts\DogFactTestCase;

class IndexControllerTest extends DogFactTestCase
{
    public function testSuccess(): void
    {
        $response = $this->getJson($this->getRouteURL());

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data',
            'current_page',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'links',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total'
        ], $response->json());
    }

    public function testSuccessWithPage(): void
    {
        $response = $this->json('GET', $this->getRouteURL(), [
            'page' => 2,
        ]);

        $response->assertStatus(200);

        $this->assertArrayHasKey('current_page', $response->json());

        $this->assertEquals(2, $response->json('current_page'));

    }

    public function testSuccessWithPageSize(): void
    {
        $response = $this->json('GET', $this->getRouteURL(), [
            'pageSize' => 15,
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'per_page' => 15,
        ]);
    }
}
